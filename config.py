#!/usr/bin/env python

# external modules
import os

# app
env = {'ON_HEROKU': os.getenv('ON_HEROKU', False),
       'TG_TOKEN': os.getenv('TG_TOKEN', False),
       'PORT': int(os.getenv('PORT', 5000)),
       'URL': os.getenv('URL', False)
       }
print(env)

#!/usr/bin/env python

# internal modules
import config
import draw

# external modules
import telebot
from telebot import types

if config.env['ON_HEROKU']:
    from flask import Flask, request
    app = Flask(__name__)

# config and variables

# app
bot = telebot.TeleBot(config.env['TG_TOKEN'])


@bot.message_handler(commands=['start', 'help'])
def start(message):
    bot.reply_to(message, f'Я бот. Приятно познакомиться, {message.from_user.first_name}')


# message handlers
@bot.message_handler(func=lambda message: True, content_types=['text'])
def echo_message(message):
    gb = draw.GBoard(5, 5)
    markup = gb.drawBord()
    bot.send_message(message.from_user.id, f'{message.from_user.first_name}, ваши очки: 0', reply_markup=markup)
    gb.add({2: {0: 'н', 1: 'а', 2: 'с', 3: 'о', 4: 'с'}})
    markup = gb.drawBord()
    bot.send_message(message.from_user.id, f'{message.from_user.first_name}, ваши очки: 0', reply_markup=markup)

    if message.text.lower() == 'привет':
        bot.send_message(message.from_user.id, 'Привет!')
    else:
        bot.send_message(message.from_user.id, 'Не понимаю, что это значит.')


# callback handlers
# ignored
@bot.callback_query_handler(func=lambda call: call.data[0:13] == 'ignore')
def ignore_inline(call):
    bot.answer_callback_query(call.id, text="")


# note parsed.
@bot.callback_query_handler(func=lambda call: call)
def note_pars(call):
    bot.answer_callback_query(call.id, text=call.data)
    pass

# router for webhook
if config.env['ON_HEROKU']:
    @app.route('/' + config.env['TG_TOKEN'], methods=['POST'])
    def get_message():
        bot.process_new_updates([telebot.types.Update.de_json(request.stream.read().decode("utf-8"))])
        return "!", 200

    @app.route("/")
    def webhook():
        bot.remove_webhook()
        bot.set_webhook(url=config.env['URL'] + config.env['TG_TOKEN'])
        return "!", 200


if __name__ == "__main__":
    if config.env['ON_HEROKU']:    
        app.run(host="0.0.0.0", port=config.env['PORT'])

    else:
        bot.remove_webhook()
        bot.polling(none_stop=True)

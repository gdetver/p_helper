from telebot import types


class GBoard:
    def __init__(self, height, width):
        self._width = width
        self._height = height
        self._width = {x for x in range(self._width)}
        self._height = {x for x in range(self._height)}
        self._board = {h: {w: ' ' for w in self._width} for h in self._height}

    def drawBord(self):
        markup = types.InlineKeyboardMarkup()
        for row in self._board:
            line = []
            print(f'[{row} : {self._board[row]}]')
            for column in self._board[row]:
                if str(self._board[row][column]) == ' ':
                    cb_data = 'ignore'
                else:
                    cb_data = str(self._board[row][column])
                line.append(types.InlineKeyboardButton(str(self._board[row][column]), callback_data=cb_data))
            markup.row(*line)
        print('Draw end \n')
        return markup

    def add(self, kwargs: dict):
        print(kwargs)
        for row in kwargs:
            for column in kwargs[row]:
                print(row, ', ', column, ' = ', kwargs[row][column])
                self._board[row][column] = kwargs[row][column]
        print('Add end \n')

        pass
